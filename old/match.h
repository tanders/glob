/* Match
*
*  Author: James Couzens <jcouzens@codeshare.ca>
*  Author: Travis Anderson <tanderson@codeshare.ca>
*
*  File:   match.h
*  Desc:   Header file for match.c
*
*  License:
*
*  The CodeShare Software License, Version 1.0
*
*  Copyright (c) 2004 James Couzens & Travis Anderson  All rights
*  reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*  1. Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*
*  2. Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED.  IN NO EVENT SHALL CODESHARE NOR ANY OF ITS ASSOCIATED
*  PROGRAMMERS OR OTHER INDIVIDUALS AFFILIATED WITH THE OWNERSHIP OR
*  OPERATION OF CODESHARE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef _MATCH_H
#define _MATCH_H


#include <sys/types.h>

#include "pattern_tables.h"


__BEGIN_DECLS


typedef unsigned char uchar;


typedef struct match_method_s
{
  char            *name;  /* the name of this match method */
  float            time;  /* time taken to run the benchmark */

  int              (*fn)(const uchar *, int, const uchar *, int);
                   /* function that implements the match algorithm */
} match_method_t;

extern match_method_t match_methods[];


int match_glob(const uchar *text, int tlen, const uchar *glob, int patternlen);
int match_glob2(const uchar *text, int tlen, const uchar *glob, int patternlen);
int match_srvx(const uchar *text, int tlen, const uchar *glob, int patternlen);
int match_ircu(const uchar *text, int tlen, const uchar *glob, int patternlen);
int match_acme(const uchar *text, int tlen, const uchar *glob, int patternlen);
int match_wild(const uchar *text, int tlen, const uchar *glob, int patternlen);

__END_DECLS


#endif /* _MATCH_H */

/* end of match.h */
