/* Match
*
*  Author: Travis Anderson <tanderson@codeshare.ca>
*
*  File:   match.c
*  Desc:   Various pattern matching functions
*
*  License:
*
*  The CodeShare Software License, Version 1.0
*
*  Copyright (c) 2004 James Couzens & Travis Anderson  All rights
*  reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*  1. Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*
*  2. Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED.  IN NO EVENT SHALL CODESHARE NOR ANY OF ITS ASSOCIATED
*  PROGRAMMERS OR OTHER INDIVIDUALS AFFILIATED WITH THE OWNERSHIP OR
*  OPERATION OF CODESHARE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*
*/


#include <stdio.h>             /* printf */
#include <sys/types.h>         /* regex */
#include <regex.h>             /* regex */
#include <ctype.h>             /* strlwr / tolower */
#include <stdlib.h>            /* malloc */
#include <string.h>            /* memset */
#include <sys/time.h>

#include "match.h"
#include "pattern_tables.h"    /* our data */


#define TOLOWER(y) str_lower_tbl[(y)]


match_method_t match_methods[] = {
  {"SRVX", 0.0f, match_srvx},
  {"GLOB", 0.0f, match_glob},
  {"GLB2", 0.0f, match_glob2},
  {"WILD", 0.0f, match_wild},
  {"SRVX", 0.0f, match_srvx},
  {"IRCU", 0.0f, match_ircu},
  //{"ACME", 0.0f, match_acme},
  {NULL,   0.0f, NULL      }
};


static uchar str_lower_tbl[] = {
    0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
   10,  11,  12,  13,  14,  15,  16,  17,  18,  19,
   20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
   30,  31,  32,  33,  34,  35,  36,  37,  38,  39,
   40,  41,  42,  43,  44,  45,  46,  47,  48,  49,
   50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
   60,  61,  62,  63,  64, 'a', 'b', 'c', 'd', 'e',
  'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
  'z',  91,  92,  93,  94,  95,  96, 'a', 'b', 'c',
  'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
  'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
  'x', 'y', 'z', 123, 124, 125, 126, 127, 128, 129,
  130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
  140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
  150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
  160, 161, 162, 163, 164, 165, 166, 167, 168, 169,
  170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
  180, 181, 182, 183, 184, 185, 186, 187, 188, 189,
  190, 191, 192, 193, 194, 195, 196, 197, 198, 199,
  200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
  210, 211, 212, 213, 214, 215, 216, 217, 218, 219,
  220, 221, 222, 223, 224, 225, 226, 227, 228, 229,
  230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
  240, 241, 242, 243, 244, 245, 246, 247, 248, 249,
  250, 251, 252, 253, 254, 255
};


/*  match_glob
 *
 *  Author: Travis Anderson <tanderson@codeshare.ca>
 *  Date:   09/18/04
 *
 *  Desc:
 *          A glob-style pattern match algorithm designed so as to not require
 *          recursion or gotos. Fast, too. patternlen isn't actually used;
 *          it's there because one of the tested algos (match_acme) requires
 *          it.
 *
 *  Notes:  1) The line referring to this note may seem odd, so I'll explain:
 *
 *               if ((#1: *tp != *gp) && (#2: TOLOWER(*tp) != TOLOWER(*gp)))
 *
 *             Comparison 1 may seem redundant, because comparison 2 handles
 *             the same occurrance.  However, if *tp == *gp, we have no need
 *             to translate the chars into their lower equivalents. So, when
 *             *tp == *gp, we already know we've failed the && test and so
 *             don't waste time with the conversion.
 */

typedef unsigned long uintptr_t;

int match_glob_better(const uchar *text, int tlen, const uchar *glob, int glen) {
  int s = 0;
  int gi = 0;
  int ti = 0;
  int gp = gi;
  int tp = ti;

  while (tp < tlen) {
    if (glob[gp] == '?') {
      gp++; tp++;
    } else if (glob[gp] == '*') {
      gp++;
      if (gp >= glen) return 1;
      else s = 1;
      ti = tp;
      gi = gp;
    } else {
      if (TOLOWER(text[tp]) != TOLOWER(glob[gp])) {
        if (s == 0) {
          return 0;
        } else {
          ti += 1;
          tp = ti;
          gp = gi;
        }
      } else {
        gp++; tp++;
      }
    }
  }

  while (glob[gp] == '*') gp++;

  return (gp == glen);
}

int match_glob_other(const uchar *text, int tlen, const uchar *glob, int glen) {
  int s = 0;
  int gi = 0;
  int ti = 0;
  int gp = gi;
  int tp = ti;

  while (tp < tlen && gp <= glen) {
    if (glob[gp] == '*') {
      if (gp + 1 >= glen) return 1;
      s = 1;
      gp += 1;
      ti = tp;
      gi = gp;
    } else {
      if (glob[gp] != '?' && TOLOWER(text[tp]) != TOLOWER(glob[gp])) {
        if (s == 0) return 0;
        ti += 1;
        tp = ti;
        gp = gi;
      } else {
        gp++; tp++;
      }
    }
  }

  while (gp < glen && glob[gp] == '*') gp++;

  return (gp == glen);
}

typedef unsigned char uint8_t;
typedef unsigned long uintptr_t;

int match_glob2(const uchar *text, int tlen, const uchar *glob, int glen) {
  uint8_t   *gp = (uint8_t *)glob;
  uint8_t   *sp = (uint8_t *)text; 
  uintptr_t  gn = glen;
  uintptr_t  sn = tlen;
  uintptr_t  st = 0;
  uintptr_t  gi = 0;
  uintptr_t  si = 0;
  uintptr_t  g  = gi;
  uintptr_t  s  = si;

  while (s < sn && g <= gn) {
    if (gp[g] == '*') {
      if (g + 1 >= gn) return 1;
      st = 1;
      g += 1;
      si = s;
      gi = g;
    } else {
      if (gp[g] != '?' && (gp[g] & ~32) != (sp[s] & ~32)) {//TOLOWER(gp[g]) != TOLOWER(sp[s])) {//(gp[g] & ~32) != (sp[s] & ~32)) {//TOLOWER(gp[g]) != TOLOWER(sp[s])) {
        if (st == 0) return 0;
        si += 1;
        s = si;
        g = gi;
      } else {
        g++; s++;
      }
    }
  }

  while (g < gn && gp[g] == '*') g++;

  return (g == gn);
}

int match_glob(const uchar *text, int tlen, const uchar *glob, int patternlen)
{
  int   in_star = 0;              /* have we found a * yet? */

  uchar *g       = (uchar *)glob; /* mask start ptr */
  uchar *t       = (uchar *)text; /* str start ptr */
  uchar *gp      = g;             /* iterative ptr for mask */
  uchar *tp      = t;             /* iterative ptr for str */

  /* we leave when we hit the end of text */
  while (*tp != 0)
  {
    switch (*gp)
    {
      case '*':
        /* go to next char in glob */
        gp++;

        /* the * is at the end of glob, we're done */
        if (*gp == 0)
        {
          return 1;
        }

        /* rememeber we found a * for later */
        in_star = 1;

        /* advance the "main" pointers to our current pos and break */
        t = tp;
        g = gp;
        break;

      default:
        /* if the two aren't equivalent at this point, we have things to do */
        /* see Note 1 */
        if (TOLOWER(*tp) != TOLOWER(*gp)) //((*tp != *gp) && (TOLOWER(*tp) != TOLOWER(*gp)))
        {
          /* if we haven't hit a * yet, we fail */
          if (in_star == 0)
          {
            return 0;
          }

          /* otherwise rewind to our last known "good" position */
          tp = ++t;
          gp = g;
          break;
        }

        /* go into the next case if we fail this if */

      case '?':
        /* increment the glob and text pointers */
        gp++;
        tp++;
    }
  }

  /* skip any trailing *s if necessary */
  while (*gp == '*')
  {
    gp++;
  }

  /* if we made it to the end of the glob, we're good (since we're also at the
     end of the text). Otherwise, we fail. */
  return (*gp == 0);
}


/*  match_wild
 *
 *  Author: Alessandro Felice Cantatore,
 *          http://xoomer.virgilio.it/acantato/dev/wildcard/
 *
 *  Desc:   The 7th iteration of Alessandro Cantatore's match algorithm (the
 *          8th depends on patterns being formatted so as to not contain
 *          consecutive *s).
 *
 */
int match_wild(const uchar *xstr, int tlen, const uchar *xpat, int patternlen)
{
  uchar *str = (uchar *)xstr;
  uchar *pat = (uchar *)xpat;
  uchar *s;
  uchar *p;
  int star = 0;

loopStart:
  for (s = str, p = pat; *s; ++s, ++p)
  {
    switch (*p)
    {
      case '?':
        break;

      case '*':
        star = 1;

        str = s;
        pat = p;

        ++pat;

        if (*pat == 0)
        {
          return 1;
        }

        goto loopStart;

      default:
        if (TOLOWER(*s) != TOLOWER(*p))
        {
          goto starCheck;
        }

        break;
    }
  }

  while (*p == '*')
  {
    ++p;
  }

  return (*p == 0);

starCheck:
   if (star == 0)
   {
     return 0;
   }

   str++;

   goto loopStart;
}


/*  match_srvx
 *
 *  Author: SrvX Development Team, www.srvx.org
 *
 *  Desc:   This is the match_ircglob() function from tools.c.  Slightly
 *          modified.
 *
 */
int match_srvx(const uchar *text, int tlen, const uchar *glob, int patternlen)
{
  unsigned int star_p = 0;
  unsigned int q_cnt  = 0;

  while (1)
  {
    switch (*glob)
    {
      case 0:
        return (*text == 0);

      case '*':
      case '?':
        star_p = q_cnt = 0;

        do {
          if (*glob == '*')
          {
            star_p = 1;
          }
          else if (*glob == '?')
          {
            q_cnt++;
          }
          else
          {
            break;
          }
          glob++;
        } while (1);

        while (q_cnt)
        {
          if (*text++ == 0)
          {
            return 0;
          }

          q_cnt--;
        }

        if (star_p)
        {
          if (*glob == 0)
          {
            return 1;
          }

          for (/* unused */; *text; text++)
          {
            if (TOLOWER(*text) == TOLOWER(*glob) &&
                match_srvx(text+1, tlen, glob+1, patternlen))
            {
              return 1;
            }
          }

          return 0;
        }

        if (*glob == 0 && *text == 0)
        {
          return 1;
        }

      default:
        if (*text == 0)
        {
          return 0;
        }

        while (*text !=  0  && *glob !=  0 &&
               *glob != '*' && *glob != '?')
        {
          if (TOLOWER(*text) != TOLOWER(*glob))
          {
            return 0;
          }
          text++;
          glob++;
        }

    } /* switch */
  } /* while */
}


/*  match_ircu
 *
 *  Author: Douglas A Lewis (dalewis@acsu.buffalo.edu)
 *
 *  Desc:   This is the match() function from ircu's match.c.  Slightly
 *          modified.
 *
 */
int match_ircu(const uchar *text, int tlen, const uchar *glob, int patternlen)
{
  int q          = 0;
  int wild       = 0;
  const uchar *g  = glob;
  const uchar *t  = text;
  const uchar *gp = glob;
  const uchar *tp = text;

  while (1)
  {
    if (*g == '*')
    {
      while (*g == '*')
      {
        g++;
      }

      wild = 1;
      gp = g;
      tp = t;
    }

    if (*g == 0)
    {
      if (*t == 0)
      {
        return 1;
      }

      for (g--; (g > glob) && (*g == '?'); g--)
      {
        /* the work is being done in the loop's header */
      }

      if ((*g == '*') && (g > glob))
      {
        return 1;
      }

      if (!wild)
      {
        return 0;
      }

      g = gp;
      t = ++tp;
    }
    else if (*t == 0)
    {
      while (*g == '*')
      {
        g++;
      }

      if (*g == 0)
      {
        return 1;
      }

      return 0;
    }

    q = 0;

    if ((TOLOWER(*g) != TOLOWER(*t)) && ((*g != '?') || q != 0))
    {
      if (wild == 0)
      {
        return 0;
      }

      g = gp;
      t = ++tp;
    }
    else
    {
      if (*g != 0)
      {
        g++;
      }

      if (*t != 0)
      {
        t++;
      }
    }
  }
}


/*  match_acme
 *
 *  Author: Jef Poskanzer (jef@acme.com)
 *
 *  Desc:   This is the match_one() function (slightly modified to fit the
 *          format of the test patterns) from thttpd's match.c.
 *
 */
int match_acme(const uchar* string, int tlen, const uchar* pattern, int patternlen)
{
  const uchar* p;

  for (p = pattern; p - pattern < patternlen; ++p, ++string)
  {
    if (*p == '?' && *string != '\0')
    {
      continue;
    }

    if (*p == '*')
    {
      int i, pl;
      ++p;

      i = strlen(string);
      pl = patternlen - (p - pattern);

      for ( ; i >= 0; --i )
      {
        if (match_acme(&(string[i]), tlen, p, pl))
        {
          return 1;
        }
      }

      return 0;
    }

    if (TOLOWER(*p) != TOLOWER(*string))
    {
      return 0;
    }
  }

  if (*string == '\0')
  {
    return 1;
  }

  return 0;
}

/* end of match.c */
