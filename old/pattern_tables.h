/* Pattern Tables
*
*  Author: James Couzens <jcouzens@codeshare.ca>
*  Author: Travis Anderson <tanderson@codeshare.ca>
*
*  File:   pattern_tables.h
*  Desc:   Pattern Tables header file
*
*  License:
*
*  The CodeShare Software License, Version 1.0
*
*  Copyright (c) 2004 James Couzens & Travis Anderson  All rights
*  reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*  1. Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*
*  2. Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED.  IN NO EVENT SHALL CODESHARE NOR ANY OF ITS ASSOCIATED
*  PROGRAMMERS OR OTHER INDIVIDUALS AFFILIATED WITH THE OWNERSHIP OR
*  OPERATION OF CODESHARE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef _PATTERN_TABLES_H
#define _PATTERN_TABLES_H 1


#include <sys/types.h>    /* standard types */


__BEGIN_DECLS


typedef struct pattern_table_s
{
  unsigned char *text;    /* string for comparison */
  unsigned char *pattern; /* regular expression */

  int   expect;  /* expected result fo matching mask against str */
} pattern_table_t;


extern pattern_table_t pattern_table_test[];
extern pattern_table_t pattern_table_bench[];


__END_DECLS


#endif /* _PATTERN_TABLES_H */

/* end of pattern_tables.h */
